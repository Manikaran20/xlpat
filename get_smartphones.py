import pandas as pd 
import numpy as np 
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import MoveTargetOutOfBoundsException
import time
import pandas as pd
from bs4 import BeautifulSoup as soup 
from selenium.webdriver.common.keys import Keys
import numpy as np
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile


options = Options()

options.headless = True
driver= webdriver.Firefox(options=options)

df=pd.DataFrame(columns= ["Smartphone","Features","Specs", "Reviews", "Pdf link"])

driver.get("https://www.verizonwireless.com/sitemap_devices.xml")
locs=driver.find_elements_by_tag_name("loc")
smartphones=[i.text for i in locs if all(['smartphones' in i.text, 'espanol' not in i.text])]

feat=[]
Specs=[]
manuals=[]
reviews=[]
names=[]

for i in smartphones[1:]:

	try:
		driver.get(i) 

		name=i.split("/")[-2]

		try:

			driver.find_element_by_xpath("//*[text()='Features']").click()
			time.sleep(0.3)

			features=driver.find_elements_by_css_selector("div.featureTitle")

			featureText=driver.find_elements_by_css_selector("div.featureDetail.padTop8")

			dct1=dict()

			for i,j in zip(features,featureText):
				dct1[i.text]=j.text
			print (dct1)

			feat.append(dct1)

		except:

			feat.append(np.nan)


		try:


			driver.find_element_by_xpath("//*[text()='Specs']").click()

			specfn=driver.find_elements_by_css_selector("div.col-sm-3.col-xs-4.specListTitle.pad10.noLeftPad")

			specfntext=driver.find_elements_by_css_selector("div.specListItem.col-sm-9.col-xs-8.pad10 ")

			dct=dict()

			for i,j in zip(specfn, specfntext):
				dct[i.text]=j.text
			Specs.append(dct)

		except:

			Specs.append(np.nan)

		try:

			manual=driver.find_element_by_xpath("//*[text()='Download user manual']")
			manuals.append(manual.get_attribute("href"))
		except:
			manuals.append(np.nan)



		try:	
			driver.find_element_by_xpath("//*[text()='Reviews']").click()

		
			review_div=driver.find_element_by_css_selector("div.col-xs-5.noSidePad.pad5")
			review=review_div.find_element_by_css_selector("div.NHaasDS75Bd").text
		except:
			review=np.nan
		reviews.append(review)

		names.append(name)

	except:
		df1=pd.concat([df, pd.DataFrame({"Smartphone":names, "Features":feat,"Specs":Specs, "Reviews":reviews, "Pdf link":manuals})])
		df1.to_csv("verizonMobileData.csv", index=False)
		break

else:

	df1=pd.concat([df, pd.DataFrame({"Smartphone":names, "Features":feat,"Specs":Specs, "Reviews":reviews, "Pdf link":manuals})])
	df1.to_csv("verizonMobileData.csv", index=False)




	
	






